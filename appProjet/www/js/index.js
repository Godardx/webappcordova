document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');
    document.getElementById('tooltip').innerHTML = `Plateforme : ${device.platform}</br>Model : ${device.model}</br>Manufacturer : ${device.manufacturer}</br>Version : ${device.version}`;

}

window.plugins.OneSignal.setAppId('8a5e6e65-e4c9-4237-a4fd-bfa0a10fa7d1');
window.plugins.OneSignal.setNotificationOpenedHandler(function(jsonData) {
    console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
});



document.getElementById('tooltip-container').addEventListener('mouseenter', displayTooltip);
document.getElementById('tooltip-container').addEventListener('mouseleave', undisplayTooltip);

function displayTooltip() {
    document.getElementById('tooltip').classList.remove('d-none');
}

function undisplayTooltip() {
    document.getElementById('tooltip').classList.add('d-none');
}

function changePage(currentPageId, newPageId) {
    document.getElementById(currentPageId).classList.replace('d-flex', 'd-none');
    document.getElementById(newPageId).classList.replace('d-none', 'd-flex');
}


function displayTops() {
    if (localStorage.length === 0) {
        document.getElementById('tops-list').innerHTML = "<p>Aucun top n'est trouvé :( </p>";
    }

    let tops = JSON.parse(localStorage.getItem('Tops'));
    let topsList = document.getElementById('tops-list');
    if (localStorage.length > 0 && localStorage.getItem('Tops') !== "") {
        topsList.innerHTML = "";
    }

    for (let i = 0; i < tops.length; i++) {
        let newDivContainer = document.createElement('div');
        let divTitle = document.createElement('div');
        let newLink = document.createElement('a');
        let newDesc = document.createElement('p');
        let deleteButton = document.createElement('a');
        let position = document.createElement('p');

        newDivContainer.classList.add('mb-3', 'd-flex', 'flex-column');
        divTitle.classList.add('d-flex', 'flex-row');

        position.innerHTML = `N°${i+1}`;
        position.classList.add('m-0');
        newLink.innerHTML = tops[i].name;
        newLink.classList.add('link');
        newLink.setAttribute('href', '#');
        newLink.setAttribute('onclick', `changePage('homepage', detailpage)`);
        deleteButton.style.color = "#eb3b5a";
        deleteButton.innerHTML = '&#x2716;';
        deleteButton.classList.add('ms-5', 'link');
        deleteButton.setAttribute('onclick', `deleteTop(${i})`);
        newDesc.innerHTML = "&#8594;" + tops[i].description;
        newDesc.classList.add('m-0');

        divTitle.appendChild(newLink);
        divTitle.appendChild(deleteButton);
        newDivContainer.appendChild(position);
        newDivContainer.appendChild(divTitle);
        newDivContainer.appendChild(newDesc);
        topsList.appendChild(newDivContainer);
    }
}