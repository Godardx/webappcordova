let addTopForm = document.getElementById('add-top-form');
let deleteTopForm = document.getElementById('delete-top-form');



function allStorage() {
    let values = [],
        keys = Object.keys(localStorage),
        i = keys.length;

    while (i--) {
        values.push(localStorage.getItem(keys[i]));
    }
    return values;
}

function createTop(e) {
    e.preventDefault();

    let globalTops = [];
    let feedback = document.getElementById('add-top-form-feedback');
    try {
        let elements = addTopForm.elements;
        let newTop = {
            'name': elements[0].value,
            'description': elements[1].value,
            'category': elements[2].value
        }

        if (localStorage.length > 0 && localStorage.getItem('Tops') !== "") {
            globalTops = JSON.parse(localStorage.getItem('Tops'));
        } else {
            globalTops = [];
        }

        console.log(globalTops);
        globalTops.push(newTop);

        if (localStorage.length === 0) {
            let storage = allStorage();
            storage.push(globalTops);
            localStorage.setItem('Tops', JSON.stringify(newTop));
        }

        localStorage.setItem('Tops', JSON.stringify(globalTops));

        feedback.style.color = "#20bf6b";
        feedback.innerHTML = "Top ajouté !";

        setInterval(() => {
            feedback.innerHTML = "";
        }, 4000);
    } catch (e) {
        feedback.style.color = "#eb3b5a";
        feedback.innerHTML = "Erreur...";
        console.log(e);

        setInterval(() => {
            feedback.innerHTML = "";
        }, 4000);
    }
}

function deleteTop(index) {

    let tops;
    if (localStorage.length > 0 && localStorage.getItem('Tops') !== "") {
        tops = JSON.parse(localStorage.getItem('Tops'));
    } else {
        tops = [];
    }

    console.log("Before delete", tops);
    delete tops[index];
    console.log("After delete", tops);

    let temp = [];
    for (let i of tops) {
        i && temp.push(i);
    }
    console.log("After filter", temp);

    localStorage.setItem('Tops', JSON.stringify(temp));
    if (localStorage.getItem('Tops') === "") {
        localStorage.clear();
        document.getElementById('')
    }

    window.location.reload(false);
}

addTopForm.addEventListener('submit', createTop);
deleteTopForm.addEventListener('submit', deleteTop);