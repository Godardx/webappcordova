# WebAppCordova

**Sujet** : Faire une web app avec cordova.

**Application mobile de création de top :**

- L’utilisateur a une page d'accueil avec la liste des tops créé.
- Lorsque je clique sur un item de la liste, j’ai une vue du top.
- Avoir un bouton de création d’un top.
- Formulaire de création d’un top avec un titre et une liste de sujet.

**Installation du projet :**

1er étape : Cloner le projet via la commande `git clone https://gitlab.com/Godardx/webappcordova.git`.

2ème étape : Ouvrir le projet qui dans le fichier "appProjet" dans un IDE.

3ème étape : Lancer un terminal dans l'IDE et exécuter les commandes suivantes :

- 1er commande : `npm i`
- 2ème commande : `cordova platfrom add browser`
- 3ème commande : `cordova build browser`
- 4ème commande : `cordova emulate browser`

**Plugin utilisiés :**

- Cordova BrowserSyncGen2
- OneSignal

Et voilà vous pouvez maintenant télécharger l'app desktop et voir le top des musiques de cette année !
